package edu.hsutx.cycle.cycleguiapp;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.TextView;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

/**
 * Created by schwierking on 4/30/18.
 */

public class Login extends AsyncTask {
        TextView statusField,roleField;
        Context context;
        private int byGetOrPost = 0;

        //flag 0 means get and 1 means post.(By default it is get.)
        public Login(Context context,TextView statusField,TextView roleField,int flag) {
            this.context = context;
            this.statusField = statusField;
            this.roleField = roleField;
            byGetOrPost = flag;
        }

        protected void onPreExecute(){
        }
        @Override
        protected Object doInBackground(Object[] arg0) {
            if(byGetOrPost == 0){ //means by Get Method

                try{
                    String username = (String)arg0[0];
                    String useremail = (String)arg0[1];

                    // this is where you put the file you want to direct too
                    //###############################################################################
                    String link = "http://csci.hsutx.edu/~manderson/cycleProject/gui_files/dbLogin.php";
                    //################################################################################
                    URL url = new URL(link);
                    HttpClient client = new DefaultHttpClient();
                    HttpGet request = new HttpGet();
                    request.setURI(new URI(link));
                    HttpResponse response = client.execute(request);
                    BufferedReader in = new BufferedReader(new
                            InputStreamReader(response.getEntity().getContent()));

                    StringBuffer sb = new StringBuffer("");
                    String line="";
                    JSONObject[] jsonObj = new JSONObject[100];


                    while ((line = in.readLine()) != null) {
                        sb.append(line);
                    }
                    String str = sb.toString();
                    String[] stringArray = str.split("&");

                    in.close();
                    for (int i = 0; i < stringArray.length; i++) {
                        jsonObj[i] = new JSONObject(stringArray[i]);
                    }

                    for(int j = 0; j < stringArray.length; j++) {
                        if (username.equals(jsonObj[j].getString("name"))) {
                            if (useremail.equals(jsonObj[j].getString("email"))) {
                                return "valid";
                            } else { return "Invalid Login"; }
                        }
                    }

                    return "Invalid Login";
                } catch(Exception e){
                    return new String("Exception: " + e.getMessage());
                }
            } else{
                try{
                    String username = (String)arg0[0];
                    String password = (String)arg0[1];

                    // this is where you put the file you want to direct too
                    //#################################################################################
                    String link="http://csci.hsutx.edu/~manderson/cycleProject/gui_files/dbLogin.php";
                    //#################################################################################
                    String data  = URLEncoder.encode("username", "UTF-8") + "=" +
                            URLEncoder.encode(username, "UTF-8");
                    data += "&" + URLEncoder.encode("password", "UTF-8") + "=" +
                            URLEncoder.encode(password, "UTF-8");

                    URL url = new URL(link);
                    URLConnection conn = url.openConnection();

                    conn.setDoOutput(true);
                    OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());

                    wr.write( data );
                    wr.flush();

                    BufferedReader reader = new BufferedReader(new
                            InputStreamReader(conn.getInputStream()));

                    StringBuilder sb = new StringBuilder();
                    String line = null;

                    // Read Server Response
                    while((line = reader.readLine()) != null) {
                        sb.append(line);
                        break;
                    }

                    return sb.toString();
                } catch(Exception e){
                    return new String("Exception: " + e.getMessage());
                }
            }
        }

        @Override
        protected void onPostExecute(Object result){
            this.statusField.setText("Login Successful");
            this.roleField.setText((String)result);
        }
}
