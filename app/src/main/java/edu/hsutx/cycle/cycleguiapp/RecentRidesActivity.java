package edu.hsutx.cycle.cycleguiapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class RecentRidesActivity extends AppCompatActivity {

    Button back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_recent_rides);
            back();
    }

    public void back() {
        back = findViewById(R.id.rr_back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent pr = new Intent(RecentRidesActivity.this,WelcomeActivity.class);
                startActivity(pr);
            }
        });
    }
}
