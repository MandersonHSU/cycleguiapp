package edu.hsutx.cycle.cycleguiapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class WelcomeActivity extends AppCompatActivity {

    Button btnLogin, btnldboard, btnpostride, btnexample;

    public void logout() {
        btnLogin = findViewById(R.id.btnLogin2);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent logout = new Intent(WelcomeActivity.this,MainActivity.class);
                startActivity(logout);
            }
        });
    }

    public void ldboard() {
        btnldboard = findViewById(R.id.btnldboard3);
        btnldboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ldboard = new Intent(WelcomeActivity.this,LeaderBoardActivity.class);
                startActivity(ldboard);
            }
        });
    }

    public void postride() {
        btnpostride = findViewById(R.id.welc_post_ride);
        btnpostride.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent pr = new Intent(WelcomeActivity.this,PostRIdeActivity.class);
                startActivity(pr);
            }
        });
    }

    public void exampleDB() {
        btnexample = findViewById(R.id.welc_recent_rides);
        btnexample.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent pr = new Intent(WelcomeActivity.this,RecentRidesActivity.class);
                startActivity(pr);
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        logout();
        ldboard();
        postride();
        exampleDB();
    }
}
