package edu.hsutx.cycle.cycleguiapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import org.json.JSONObject;

public class LeaderBoardActivity extends AppCompatActivity {
    Button back, btnpostride;
    TextView lbtxtName1,lbtxtName2,lbtxtName3,lbtxtName4,lbtxtName5;
    TextView lbtxtDis1,lbtxtDis2,lbtxtDis3,lbtxtDis4,lbtxtDis5;
    TextView lbtxtTt1,lbtxtTt2,lbtxtTt3,lbtxtTt4,lbtxtTt5;
    TextView status,role;
    String ldbord;

    String username,pass;

    public void rtnmain() {
        back = findViewById(R.id.btnldmain);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent rtn = new Intent(LeaderBoardActivity.this, WelcomeActivity.class);
                startActivity(rtn);
            }
        });
    }

    public void postride() {
        btnpostride = findViewById(R.id.btnpr);
        btnpostride.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent pr = new Intent(LeaderBoardActivity.this, PostRIdeActivity.class);
                startActivity(pr);
            }
        });
    }

    public void ldboard() {
        try {

            username = "bhavik";
            pass = "pass";
            lbtxtName1 = findViewById(R.id.lbtxtnm1);
            lbtxtDis1 = findViewById(R.id.lbtxtdis1);
            lbtxtTt1 = findViewById(R.id.lbtxttt1);

            lbtxtName2 = findViewById(R.id.txtnm2);
            lbtxtDis2 = findViewById(R.id.txtdis2);
            lbtxtTt2 = findViewById(R.id.txttt2);

            lbtxtName3 = findViewById(R.id.txtnm3);
            lbtxtDis3 = findViewById(R.id.txtdis3);
            lbtxtTt3 = findViewById(R.id.txttt3);

            lbtxtName4 = findViewById(R.id.txtnm4);
            lbtxtDis4 = findViewById(R.id.txtdis4);
            lbtxtTt4 = findViewById(R.id.txttt4);

            lbtxtName5 = findViewById(R.id.txtnm5);
            lbtxtDis5 = findViewById(R.id.txtdis5);
            lbtxtTt5 = findViewById(R.id.txttt5);

            status = findViewById(R.id.demoview1);
            role = findViewById(R.id.demoview2);

            new LeaderBoard(this, status, role, 0,lbtxtName1,lbtxtName2,lbtxtName3,lbtxtName4,lbtxtName5,
                    lbtxtDis1,lbtxtDis2,lbtxtDis3,lbtxtDis4,lbtxtDis5,
                    lbtxtTt1,lbtxtTt2,lbtxtTt3,lbtxtTt4,lbtxtTt5).execute(username, pass);

        } catch(Exception e){
            System.out.println("Error: Unable to load leaderboard");
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leader_board);

        //WebView myWebView = findViewById(R.id.ldWebView);
        //myWebView.loadUrl("http://csci.hsutx.edu/~bdpatel/cycleProject/public/ldboard");
        rtnmain();
        postride();
        ldboard();
    }
}