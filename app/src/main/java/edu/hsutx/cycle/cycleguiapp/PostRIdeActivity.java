package edu.hsutx.cycle.cycleguiapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class PostRIdeActivity extends AppCompatActivity {

    Button mpbtnpostride;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_ride);
        postride();
    }
    public void postride() {
        mpbtnpostride = findViewById(R.id.mpbtnmap);
        mpbtnpostride.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent pr = new Intent(PostRIdeActivity.this, MapsActivity.class);
                startActivity(pr);
            }
        });
    }
}
