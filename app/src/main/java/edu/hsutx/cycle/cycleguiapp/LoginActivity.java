package edu.hsutx.cycle.cycleguiapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class LoginActivity extends AppCompatActivity {

    EditText usernameField,emailField;
    TextView status,role;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        usernameField = findViewById(R.id.editUsername2);
        emailField = findViewById(R.id.editEmail);

        Button btnLogin = findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
            }
        });
        //WebView myWebView = findViewById(R.id.webView);
        //myWebView.loadUrl("http://csci.hsutx.edu/~hrschwierking/cycleProject/public/login");
    }

    public void login() {
        String username = usernameField.getText().toString();
        String email = emailField.getText().toString();
        status = findViewById(R.id.textView11);
        role = findViewById(R.id.textView10);

        new Login(this,status,role,0).execute(username,email);

        if (role.getText().equals("valid")) {
            Intent login = new Intent(LoginActivity.this,WelcomeActivity.class);
            startActivity(login);
        }
    }
}
