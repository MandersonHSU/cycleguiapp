package edu.hsutx.cycle.cycleguiapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


//public class MainActivity extends Activity {

public class exampleLoginActivity extends AppCompatActivity {

    Button backbtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_example_login);
        back();
    }

    public void back() {
        backbtn = findViewById(R.id.aboutback);
        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent MainActivity = new Intent(exampleLoginActivity.this,MainActivity.class);
                startActivity(MainActivity);
            }
        });
    }

}