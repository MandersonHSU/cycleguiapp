package edu.hsutx.cycle.cycleguiapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.TextView;

public class LeaderBoard  extends AsyncTask{
    TextView lbtxtName1,lbtxtName2,lbtxtName3,lbtxtName4,lbtxtName5;
    TextView lbtxtDis1,lbtxtDis2,lbtxtDis3,lbtxtDis4,lbtxtDis5;
    TextView lbtxtTt1,lbtxtTt2,lbtxtTt3,lbtxtTt4,lbtxtTt5;
    TextView statusField,roleField;
    Context context;
    private int byGetOrPost = 0;

    //flag 0 means get and 1 means post.(By default it is get.)
    public LeaderBoard(Context context,TextView statusField,TextView roleField,int flag,
                       TextView nm1,TextView nm2,TextView nm3,TextView nm4,TextView nm5,
                       TextView Dis1,TextView Dis2,TextView Dis3,TextView Dis4,TextView Dis5,
                       TextView tt1,TextView tt2,TextView tt3,TextView tt4,TextView tt5) {
        this.context = context;
        this.statusField = statusField;
        this.roleField = roleField;
        byGetOrPost = flag;
        this.lbtxtName1 = nm1;
        this.lbtxtName2 = nm2;
        this.lbtxtName3 = nm3;
        this.lbtxtName4 = nm4;
        this.lbtxtName5 = nm5;
        this.lbtxtDis1 = Dis1;
        this.lbtxtDis2 = Dis2;
        this.lbtxtDis3 = Dis3;
        this.lbtxtDis4 = Dis4;
        this.lbtxtDis5 = Dis5;
        this.lbtxtTt1 = tt1;
        this.lbtxtTt2 = tt2;
        this.lbtxtTt3 = tt3;
        this.lbtxtTt4 = tt4;
        this.lbtxtTt5 = tt5;
    }

    protected void onPreExecute(){
    }
    @Override
    protected Object doInBackground(Object[] arg0) {
        if(byGetOrPost == 0){ //means by Get Method

            try{
                String username = (String)arg0[0];
                String password = (String)arg0[1];

                // this is where you put the file you want to direct too
                //###############################################################################
                String link = "http://csci.hsutx.edu/~manderson/cycleProject/gui_files/dbLeaderBoard.php";
                //################################################################################
                URL url = new URL(link);
                HttpClient client = new DefaultHttpClient();
                HttpGet request = new HttpGet();
                request.setURI(new URI(link));
                HttpResponse response = client.execute(request);
                BufferedReader in = new BufferedReader(new
                        InputStreamReader(response.getEntity().getContent()));

                StringBuffer sb = new StringBuffer("");
                String line="";
                JSONObject[] jsonObj = new JSONObject[100];

                while ((line = in.readLine()) != null) {
                    sb.append(line);
                }

                String str = sb.toString();
                String[] stringArray = str.split("&");
                in.close();
                for (int i = 0; i < stringArray.length; i++) {
                    jsonObj[i] = new JSONObject(stringArray[i]);
                }

                this.lbtxtName1.setText(jsonObj[0].getString("name"));
                this.lbtxtDis1.setText(jsonObj[0].getString("distance"));
                this.lbtxtTt1.setText(jsonObj[0].getString("no_of_trips"));

                this.lbtxtName2.setText(jsonObj[1].getString("name"));
                this.lbtxtDis2.setText(jsonObj[1].getString("distance"));
                this.lbtxtTt2.setText(jsonObj[1].getString("no_of_trips"));

                this.lbtxtName3.setText(jsonObj[2].getString("name"));
                this.lbtxtDis3.setText(jsonObj[2].getString("distance"));
                this.lbtxtTt3.setText(jsonObj[2].getString("no_of_trips"));

                this.lbtxtName4.setText(jsonObj[3].getString("name"));
                this.lbtxtDis4.setText(jsonObj[3].getString("distance"));
                this.lbtxtTt4.setText(jsonObj[3].getString("no_of_trips"));

                this.lbtxtName5.setText(jsonObj[4].getString("name"));
                this.lbtxtDis5.setText(jsonObj[4].getString("distance"));
                this.lbtxtTt5.setText(jsonObj[4].getString("no_of_trips"));


                return sb.toString();
            } catch(Exception e){
                return new String("Exception: " + e.getMessage());
            }
        } else{
            try{
                String username = (String)arg0[0];
                String password = (String)arg0[1];

                // this is where you put the file you want to direct too
                //#################################################################################
                String link="http://csci.hsutx.edu/~manderson/cycleProject/gui_files/dbUtils.php";
                //#################################################################################
                String data  = URLEncoder.encode("username", "UTF-8") + "=" +
                        URLEncoder.encode(username, "UTF-8");
                data += "&" + URLEncoder.encode("password", "UTF-8") + "=" +
                        URLEncoder.encode(password, "UTF-8");

                URL url = new URL(link);
                URLConnection conn = url.openConnection();

                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());

                wr.write( data );
                wr.flush();

                BufferedReader reader = new BufferedReader(new
                        InputStreamReader(conn.getInputStream()));

                StringBuilder sb = new StringBuilder();
                String line = null;

                // Read Server Response
                while((line = reader.readLine()) != null) {
                    sb.append(line);
                    break;
                }

                return sb.toString();
            } catch(Exception e){
                return new String("Exception: " + e.getMessage());
            }
        }
    }

    @Override
    protected void onPostExecute(Object result){
        this.statusField.setText("Login Successful");
        this.roleField.setText((String)result);

    }
}